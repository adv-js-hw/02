const books = [
  { author: "Люсі Фолі", name: "Список запрошених", price: 70 },
  { author: "Сюзанна Кларк", name: "Джонатан Стрейндж і м-р Норрелл" },
  { name: "Дизайн. Книга для недизайнерів.", price: 70 },
  { author: "Алан Мур", name: "Неономікон", price: 70 },
  { author: "Террі Пратчетт", name: "Рухомі картинки", price: 40 },
  { author: "Анґус Гайленд", name: "Коти в мистецтві" },
  { author: "Ден Браун", price: 70 },
];

// Перевірка на валідність необхідним умовам:
function validateBooks(book) {
  // 1st OPTION:
  return book.hasOwnProperty("author") && book.hasOwnProperty("name") &&
    book.hasOwnProperty("price");

  // // 2nd OPTION: 
  // return book.author && book.name && book.price;
}

function renderBooks() {
  const root = document.createElement("div");
  document.body.append(root);


  const ul = document.createElement("ul");
  ul.innerHTML = "<b>List of books for SALE:</b><br><br>";
  ul.style.fontSize = "24px"

  books.forEach((book) => {
    if (validateBooks(book)) {
      const li = document.createElement("li");
      li.textContent = `${book.author} "${book.name.toUpperCase()}": ${
        book.price
      } грн.`;
      li.style.fontSize = "20px"
      ul.append(li);
    } else if (!book.author) {
      try {
        throw new Error(
          `The book "${book.name}", worth ${book.price} grn., has no author`
        );
      } catch (err) {
        console.error(err.message);
      }
    } else if (!book.name) {
      try {
        throw new Error(
          `The book author by ${book.author}, worth ${book.price} grn., has no title`
        );
      } catch (err) {
        console.error(err.message);
      }
    } else if (!book.price) {
      try {
        throw new Error(
          `The book "${book.name}" by ${book.author} has no price`
        );
      } catch (err) {
        console.error(err.message);
      }
    }

    // // BASIC METHOD:
    // else if (!book.author) {
    //   console.error(`The book "${book.name}", worth ${book.price} grn., has no author`);
    // }
    // else if (!book.name) {
    //   console.error(`The book author by ${book.author}, worth ${book.price} grn., has no title`);
    // }
    // else if (!book.price) {
    //   console.error(`The book "${book.name}" by ${book.author} has no price`);
    // }

    // ANOTHER METHOD:
    // else {
    //   // throw new Error(`Something gone wrong: ${JSON.stringify(book)}`);
    //   console.error(`Something gone wrong: ${JSON.stringify(book)}`);
    // }
  });

  root.append(ul);
}

renderBooks();
